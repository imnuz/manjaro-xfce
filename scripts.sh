#!/bin/bash

FILES="$(pwd)/files"

install_packages (){
  if ! [ -x "$(command -v rofi)" ]; then
    yay -S --noconfirm --nocleanmenu --nodiffmenu google-chrome firefox falkon youtube-dl rofi gedit transmission-gtk \
      flameshot neovim peek mpv plank tmux hardinfo ntp \
      asciinema net-tools cmus imagewriter alacritty ripgrep lf
  fi

  read -p ' > Install development packages(y/N): ' ans
  if [ "$ans" == "y" ]; then
      yay -S --noconfirm --nocleanmenu --nodiffmenu pycharm-professional dbeaver yed tusk
  fi
}


set_vim (){
  read -p ' > Set vim configuration (y/N): ' ans
  if [ "$ans" == "y" ]; then
    echo " > Set vim"
    cp "${FILES}/vim/vimrc" ~/.vimrc
    echo "alias vi=nvim" >> $HOME/.zshrc
    echo "alias vim=nvim" >> $HOME/.zshrc
    echo "export EDITOR=nvim" >> $HOME/.zshrc
  fi
}

set_docker (){
  read -p ' > Set Docker (y/N): ' ans
  if [ "$ans" == "y" ]; then
    yay -S --noconfirm --nocleanmenu --nodiffmenu docker docker-compose
    sudo usermod -aG docker $USER
    sudo systemctl enable docker
    sudo systemctl start docker
  fi
}


set_timezone (){
  read -p ' > Set time (y/N): ' ans
  if [ "$ans" == "y" ]; then
    sudo timedatectl set-ntp true
    sudo timedatectl set-timezone Asia/Seoul
    timedatectl status
  fi
}


install_virtualbox (){
  if ! [ -x "$(command -v virtualbox)" ]; then
	  echo " > virtualbox"
	  lk=$(mhwd-kernel -li | grep ' linux' | awk '{ print $2 }')
	  yay -S --noconfirm --nocleanmenu --nodiffmenu  virtualbox ${lk}-virtualbox-host-modules ${lk}-virtualbox-guest-modules
	  sudo vboxreload
	  echo "visit http://download.virtualbox.org/virtualbox/6.0.12/ to downlaod guest-iso file."
  fi
}

config_input (){
  read -p ' > Set scim (y/N): ' ans
  if [ "$ans" == "y" ]; then
    yay -S --noconfirm --nocleanmenu --nodiffmenu scim scim-hangul ttf-d2coding
    sudo locale-gen
    locale -a
    localectl set-locale LANG=en_US.UTF-8
    scim-setup
    echo "export XMODIFIERS=\"@im=SCIM\"" >> ~/.profile
    echo "export GTK_IM_MODULE=scim" >> ~/.profile
    echo "export QT_IM_MODULE=scim" >> ~/.profile
    echo "scim -d" >> ~/.profile

    echo " > Set Mouse"
    sudo sed -i 's/pointer catchall"/pointer catchall"\n\tOption "NaturalScrolling" "true"/' /usr/share/X11/xorg.conf.d/40-libinput.conf

    echo " > Set Keyboard"
    sudo cp "${FILES}/xfce/keyboards.xml" ~/.config/xfce4/xfconf/xfce-perchannel-xml/
    sudo cp "${FILES}/xfce/xfce4-keyboard-shortcuts.xml" ~/.config/xfce4/xfconf/xfce-perchannel-xml
    killall xfconfd
    /usr/lib/xfce4/xfconf/xfconfd &
    xfsettingsd --replace &
	fi
}


set_plank (){
  read -p ' > Set plank (y/N): ' ans
  if [ "$ans" == "y" ]; then
    plank --preferences & 
    cp "${FILES}/plank.desktop" ~/.config/autostart/plank.desktop
  fi
}

set_ssh (){
  read -p ' > Set ssh (y/N): ' ans
  if [ "$ans" == "y" ]; then
    yay --noconfirm --nocleanmenu --nodiffmenu -S openssh
    sudo systemctl enable sshd.service
    sudo systemctl start sshd.service
  fi
}

set_python (){
  if ! [ -x "$(command -v pyenv)" ]; then
    yay --noconfirm --nocleanmenu --nodiffmenu -S pyenv direnv pyenv-virtualenv
    pyenv install 2.7.16
    pyenv install 3.7.4
  fi
}

git_config (){
  read -p ' > Set git config (y/N): ' ans
  if [ "$ans" == "y" ]; then
    git config --global user.email kinuzy@gmail.com
    git config --global user.name Donghun Kim
	fi
}

set_conky (){
  read -p ' > Set conky (y/N): ' ans
  if [ "$ans" == "y" ]; then
	  mkdir -p ~/.config/conky
    cp "${FILES}/conky.conf" ~/.config/conky
    echo "(sleep 2s && conky) &" >> ~/.config/autostart/conky.desktop
  fi
}


install_packages
set_vim
set_docker
config_input
git_config
set_plank
set_ssh
set_conky
install_virtualbox

mkdir -p ~/.config/terminator
cp "${FILES}/terminator-config" ~/.config/terminator/config
cp "${FILES}/xfce/terminalrc" ~/.config/xfce4/terminal/
cp "${FILES}/xfce/xsettings.xml" ~/.config/xfce4/xfconf/xfce-perchannel-xml/
cp "${FILES}/xfce/xfce4-panel.xml" ~/.config/xfce4/xfconf/xfce-perchannel-xml/

