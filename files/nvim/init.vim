"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"
" - Vim Awesome
"   => https://vimawesome.com/
"
" - Vim Cheat Sheet
"  => https://vim.rtorr.com/lang/ko/
"
" https://github.com/nickjj/dotfiles/blob/master/.vimrc
" http://jaeheeship.github.io/console/2013/11/15/vimrc-configuration.html
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <Space> <Leader>

syntax enable
set nocompatible
set hidden
set nowrap
set smarttab
set pumheight=10
set smartindent
set autoindent
set cursorline
set relativenumber
set showtabline
set clipboard=unnamedplus
set smarttab
set cindent
set tabstop=4
set shiftwidth=4
set expandtab
set hidden 
set updatetime=300
set shortmess+=c
set signcolumn=yes
set encoding=UTF-8
set nomodeline
set splitbelow splitright

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

map <Leader>tt :vs term://zsh<CR>
set fillchars+=vert:\ 
tnoremap <Esc> <C-\><C-n>

map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

"
"" Highlight the active window even play nice with tmux splits
"
if has('nvim')
    hi ActiveWindow ctermbg=00 | hi InactiveWindow ctermbg=235
    set winhighlight=Normal:ActiveWindow,NormalNC:InactiveWindow
    au VimEnter,WinEnter,BufEnter,BufWinEnter,FocusGained * hi ActiveWindow ctermbg=00 | hi InactiveWindow ctermbg=236
    au VimLeave,WinLeave,BufLeave,BufWinLeave,FocusLost * hi ActiveWindow ctermbg=236 | hi InactiveWindow ctermbg=236
else
    hi Normal ctermbg=None
endif


call plug#begin('~/.local/share/nvim/plugged')


" ===================================================================================
" Vim Theme
" -----------------------------------------------------------------------------------
Plug 'morhetz/gruvbox'


" ===================================================================================
" Nerd Commenter
" -----------------------------------------------------------------------------------
Plug 'scrooloose/nerdcommenter'
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDAltDelims_java = 1
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1


" ===================================================================================
" fzf
" -----------------------------------------------------------------------------------
nnoremap <C-p> :Files<Cr>
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'


" ===================================================================================
" rainbow_parentheses
" -----------------------------------------------------------------------------------
Plug 'junegunn/rainbow_parentheses.vim'
let g:rainbow#max_level = 16
let g:rainbow#pairs = [['(', ')'], ['[', '']]
" List of colors that you do not want. ANSI code or #RRGGBB
let g:rainbow#blacklist = [233, 234]


" ===================================================================================
" Vim Smooth Scroll
" -----------------------------------------------------------------------------------
Plug 'terryma/vim-smooth-scroll'
noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 30, 3)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 30, 3)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 20, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 20, 4)<CR>


" ===================================================================================
" vim tmux navigator
" -----------------------------------------------------------------------------------
Plug 'christoomey/vim-tmux-navigator'
" Use default configuration


" ===================================================================================
" Vim Git Gutter
" -----------------------------------------------------------------------------------
Plug 'airblade/vim-gitgutter'
function! GitStatus()
    let [a,m,r] = GitGutterGetHunkSummary()
    return printf('+%d ~%d -%d', a, m, r)
endfunction
set statusline+=%{GitStatus()}

"
" ===================================================================================
" Vim Fugitive => https://github.com/tpope/vim-fugitive
" -----------------------------------------------------------------------------------
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
:set diffopt+=vertical

" Plug 'zivyangll/git-blame.vim'
" nnoremap <Leader>s :<C-u>call gitblame#echo()<CR>
Plug 'tommcdo/vim-fugitive-blame-ext'


" ===================================================================================
" lightline => https://github.com/itchyny/lightline.vim
" -----------------------------------------------------------------------------------
Plug 'itchyny/lightline.vim'
Plug 'sinetoami/lightline-hunks'
set laststatus=2
let g:lightline = {
            \'colorscheme': 'one',
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ],
            \               ['lightline_hunks'],
            \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ],
            \ },
            \ 'component_function': {
            \   'gitbranch': 'FugitiveHead',
            \   'filename': 'LightLineFilename',
            \  'lightline_hunks': 'lightline#hunks#composer',
            \ },
            \}
let g:lightline.separator = {
            \   'left': '', 'right': ''
            \}
let g:lightline.subseparator = {
            \   'left': '', 'right': '' 
            \}
let g:lightline#hunks#hunk_symbols = [ 'A:', 'M:', 'R:' ]
let g:lightline#hunks#exclude_filetypes = [ 'startify', 'nerdtree', 'vista_kind', 'tagbar' ]

function! LightLineFilename()
    return expand('%')
endfunction

Plug 'mengelbrecht/lightline-bufferline'
set showtabline=2  " Show tabline
set guioptions-=e  " Don't use GUI tabline
let g:lightline.tabline          = {'left': [['buffers']], 'right': [['close']]}
let g:lightline.component_expand = {'buffers': 'lightline#bufferline#buffers'}
let g:lightline.component_type   = {'buffers': 'tabsel'}


" ===================================================================================
" NerdTree => https://github.com/scrooloose/nerdtree-git-plugin
" -----------------------------------------------------------------------------------
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

nmap <Leader>n :NERDTreeToggle<CR>
let NERDTreeIgnore = ['\.pyc$', '^__pycache__$']


" ===================================================================================
" Indent Line => https://github.com/Yggdroot/indentLine
" -----------------------------------------------------------------------------------
Plug 'Yggdroot/indentLine'
let g:indentLine_setColors = 0
let g:indentLine_char_list = ['|', '¦', '┆', '┊']


" ===================================================================================
" Indent Line => https://github.com/Yggdroot/indentLine
" -----------------------------------------------------------------------------------
" Plug 'vim-syntastic/syntastic'
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
" let g:syntastic_python_checkers = ['pylint']


" ===================================================================================
" Mutliple Cursors => https://github.com/terryma/vim-multiple-cursors
" -----------------------------------------------------------------------------------
Plug 'terryma/vim-multiple-cursors'


Plug 'easymotion/vim-easymotion'
Plug 'Raimondi/delimitMate'

" ===================================================================================
" TagBar => https://github.com/majutsushi/tagbar
" -----------------------------------------------------------------------------------
"Plug 'majutsushi/tagbar'
" nmap <F8> :TagbarToggle<CR>

"Plug 'sakhnik/nvim-gdb', { 'do': ':!./install.sh \| UpdateRemotePlugins' }
"Plug 'SkyLeach/pudb.vim'




" ===================================================================================
" COC => https://github.com/neoclide/coc.nvim
"   - Install : https://github.com/neoclide/coc.nvim/wiki/Install-coc.nvim
"   - Extensions: https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions
" -----------------------------------------------------------------------------------
Plug 'neoclide/coc.nvim', {'branch': 'release'}

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
      let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~# '\s'
    endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" inoremap <silent><expr> <TAB>
"             \ pumvisible() ?  "<C-n>" :
"             \ coc#expandableOrJumpable() ? coc#rpc#request('doKeymap', ['snippets-expand-jump','']) :
"             \ <SID>check_back_space() ? "\<TAB>" :
"             \ coc#refresh()
" function! s:check_back_space() abort
"     let col = col('.') - 1
"     return !col || getline('.')[col - 1]  =~# '\s'
" endfunction

let g:coc_snippet_next = '<tab>'

autocmd FileType json syntax match Comment +\/\/.\+$+


" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
"
" " GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)]

nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" ===================================================================================
" TIG => https://github.com/iberianpig/tig-explorer.vim
" -----------------------------------------------------------------------------------
"Plug 'iberianpig/tig-explorer.vim'
"Plug 'rbgrouleff/bclose.vim'



"Plug 'othree/javascript-libraries-syntax.vim'
"Plug 'posva/vim-vue'
"let g:used_javascript_libs = 'vue,jquery'


" ===================================================================================
" Vim Test => https://github.com/janko/vim-test
" -----------------------------------------------------------------------------------
Plug 'janko/vim-test'
let test#strategy='neovim'
let test#python#runner='pytest'
nmap <silent> t<C-n> :TestNearest<CR>
nmap <silent> t<C-f> :TestFile<CR>
nmap <silent> t<C-s> :TestSuite<CR>
nmap <silent> t<C-l> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>
Plug 'vim-vdebug/vdebug'



Plug 'dense-analysis/ale'
let g:ale_linters = {'python': ['flake8']}
let g:ale_fixers = {'*':[], 'python':['black']}
let g:ale_fix_on_save = 1



Plug 'ekalinin/Dockerfile.vim'
Plug 'chr4/nginx.vim'

" Initialize plugin system
call plug#end()
"####################################################################################################

colorscheme gruvbox

"####################################################################################################
" Nvim documentation: provider
"   => https://neovim.io/doc/user/provider.html
"####################################################################################################
let g:python_host_prog = '~/.pyenv/versions/pynvim/bin/python'
let g:python3_host_prog = '~/.pyenv/versions/py3nvim/bin/python'


