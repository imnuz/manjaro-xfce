#!/bin/bash

FILES="$(pwd)/files"

install_pkgs () {
  sudo cp ${FILES}/mirrorlist /etc/pacman.d
  if ! [ -x "$(command -v yay)" ]; then
    sudo pacman -Syyu --noconfirm
    sudo pacman -S --noconfirm base-devel --needed
    sudo pacman -S --noconfirm yay the_silver_searcher
  fi
  chsh -s `which zsh`
}

# install Oh My Zsh
# install PowerLevel9k
install_ohmyzsh () {
	read -p ' > Install Oh My Zsh(Y/n): ' ans
	if [ "$ans" == "y" ]; then
		sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh) --unattended"
		sed -i 's/plugins=(git)/plugins=( archlinux git history-substring-search pyenv virtualenv django colored-man-pages zsh-autosuggestions zsh-syntax-highlighting )/' ~/.zshrc
		git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
		sed -i 's|robbyrussell|powerlevel9k/powerlevel9k|' ~/.zshrc
	fi

	read -p ' > Install Oh My Zsh Plugins (Y/n): ' ans
	if [ "$ans" == "y" ]; then
		cd ~/.oh-my-zsh/plugins/
		git clone https://github.com/zsh-users/zsh-autosuggestions
		git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
	fi
}

# Install nerd fonts
#	 - https://www.youtube.com/watch?v=iwH1XqVjZOE
#  - https://www.youtube.com/watch?v=UsKd9Y42Mo0
#  - https://github.com/Powerlevel9k/powerlevel9k/wiki/Stylizing-Your-Prompt
#  - https://www.youtube.com/watch?v=wM1uNqj71Ko
install_nerd_fonts () {
	read -p ' > Install Nerd fonts (Y/n): ' ans
	if [ "$ans" == "y" ]; then
#cd ~/.local && git clone https://github.com/ryanoasis/nerd-fonts.git && ./nerd-fonts/install.sh Meslo
		mkdir -p ~/.local/share/fonts/NerdFonts
		cp ${FILES}/fonts/* ~/.local/share/fonts/NerdFonts
		fc-cache
	fi
	# http://mayccoll.github.io/Gogh/
	bash -c  "$(wget -qO- https://git.io/vQgMr)"
}

# Install FZF
#  - https://github.com/junegunn/fzf
install_fzf (){
	read -p ' > Install fzf  (Y/n): ' ans
	if [ "$ans" == "y" ]; then
	  sudo pacman -Syu --noconfirm fd
		git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
		~/.fzf/install
	fi
}

install_pkgs
install_ohmyzsh
install_nerd_fonts
install_fzf

echo 'source ~/workspace/manjaro-xfce/files/zshrc' >> ~/.zshrc
